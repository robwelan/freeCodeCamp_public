import React from 'react';
import PropTypes from 'prop-types';

// very simple lazy load
const LazyLoadImages = (props) => {
  const { images } = props;
  /* These images are for presentation only.
    No need to worry about index uniqueness.
  */

  // eslint-disable-next-line
  const imageMap = images.map((i, index) => <img className="hidden-lazy-load-image" src={i} alt="" key={index} />);

  return (
    <React.Fragment>
      <style>
        {
          `
          .hidden-lazy-load-image {
            display: none;
            width: 1px;
            height: 1px;
          }
          `
        }
      </style>
      {imageMap}
    </React.Fragment>
  );
};

LazyLoadImages.propTypes = {
  images: PropTypes.arrayOf(PropTypes.string).isRequired,
};

export default LazyLoadImages;
