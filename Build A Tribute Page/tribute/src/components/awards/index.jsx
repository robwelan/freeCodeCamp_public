import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
// Custom Components
import ListItems from './items';
// Utilities
import sorting from '../../utilities/awards';

const styles = theme => ({
  paper: {
    color: theme.palette.text.secondary,
    padding: theme.spacing.unit * 2,
    textAlign: 'justify',
  },
  root: {
    flexGrow: 1,
  },
});

const Awards = (props) => {
  const { awards, classes } = props;

  awards.sort(sorting.descending);

  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={1} md={2} />
        <Grid item xs={10} md={8}>
          <Paper className={classes.paper}>
            <Typography variant="headline" gutterBottom>
              Awards
            </Typography>
            <List>
              <ListItems
                awards={awards}
              />
            </List>
          </Paper>
        </Grid>
        <Grid item xs={1} md={2} />
      </Grid>
    </div>
  );
};

Awards.propTypes = {
  awards: PropTypes.arrayOf(
    PropTypes.shape({}).isRequired,
  ).isRequired,
  classes: PropTypes.shape({
    paper: PropTypes.string.isRequired,
    root: PropTypes.string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(Awards);
