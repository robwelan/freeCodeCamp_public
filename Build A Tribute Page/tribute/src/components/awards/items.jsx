import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import StarIcon from '@material-ui/icons/Star';
import Divider from '@material-ui/core/Divider';

const styles = {
  avatar: {
    margin: 10,
  },
};

const ListItems = (props) => {
  const {
    classes,
    awards,
  } = props;

  const itemMap = awards.map((ex, index) => // eslint-disable-next-line
    /* These items are for presentation only.
        No need to worry about index uniqueness.
    */

    // eslint-disable-next-line
    <React.Fragment key={index}>
      <ListItem>
        <Avatar className={classes.avatar}>
          <StarIcon />
        </Avatar>
        <ListItemText
          primary={
            `
                ${ex.award}
                ${' - '}
                ${ex.by}
              `
          }
          secondary={ex.year}
        />
      </ListItem>
      <li>
        <Divider inset />
      </li>
    </React.Fragment>);

  return (
    <React.Fragment>
      {itemMap}
    </React.Fragment>
  );
};

ListItems.propTypes = {
  awards: PropTypes.arrayOf(
    PropTypes.shape({}).isRequired,
  ).isRequired,
  classes: PropTypes.shape({
    avatar: PropTypes.string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(ListItems);
