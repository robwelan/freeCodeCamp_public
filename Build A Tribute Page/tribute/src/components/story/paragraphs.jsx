import React from 'react';
import ReactHtmlParser from 'react-html-parser';
import Typography from '@material-ui/core/Typography';
import './index.css';

/* These paragraphs are for presentation only.
   No need to worry about index uniqueness.
*/

// eslint-disable-next-line
const Paragraphs = props => props.paragraphs.map((s, index) => <Typography gutterBottom align="justify" key={index} className="story">{ReactHtmlParser(s)}</Typography>);

export default Paragraphs;
