import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
// custom components
import Paragraphs from './paragraphs';

const styles = theme => ({
  paper: {
    color: theme.palette.text.secondary,
    padding: theme.spacing.unit * 2,
    textAlign: 'justify',
  },
  root: {
    flexGrow: 1,
  },
});

const Story = (props) => {
  const { classes, story } = props;
  const paragraphs = story.split('<br/>');

  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={1} md={2} />
        <Grid item xs={10} md={8}>
          <Paper className={classes.paper}>
            <Typography variant="headline" gutterBottom>
              Background
            </Typography>
            <Paragraphs paragraphs={paragraphs} />
          </Paper>
        </Grid>
        <Grid item xs={1} md={2} />
      </Grid>
    </div>
  );
};

Story.propTypes = {
  classes: PropTypes.shape({
    paper: PropTypes.string.isRequired,
    root: PropTypes.string.isRequired,
  }).isRequired,
  story: PropTypes.string.isRequired,
};

export default withStyles(styles)(Story);
