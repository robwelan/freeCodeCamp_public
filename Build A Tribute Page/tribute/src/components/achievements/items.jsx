import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
// Custom utilities
import getImageLocation from '../../utilities/images';
// Custom styles
import './items.css';

const styles = {
  avatar: {
    margin: 10,
  },
};

const ListItems = (props) => {
  const {
    classes,
    experiences,
    maxYear,
    images,
  } = props;

  const itemMap = experiences.map((ex, index) => {
    const imageLocation = getImageLocation(images);

    return (// eslint-disable-next-line
      /* These items are for presentation only.
          No need to worry about index uniqueness.
      */

      // eslint-disable-next-line
      <React.Fragment key={index}>
        <ListItem>
          <Avatar
            alt={ex.logo.alt}
            src={`${imageLocation}${ex.logo.file}`}
            className={classes.avatar}
          />
          <ListItemText
            primary={ex.acomplishment.title}
            secondary={
              `
                ${ex.start.year}
                ${' to '}
                ${ex.end.year === maxYear ? 'Current' : ex.end.year}
              `
            }
          />
        </ListItem>
        <ListItem>
          <ListItemText
            className="indent-more"
            inset
            secondary={ex.acomplishment.activity}
          />
        </ListItem>
        <li>
          <Divider inset />
        </li>
      </React.Fragment>
    );
  });

  return (
    <React.Fragment>
      {itemMap}
    </React.Fragment>
  );
};

ListItems.propTypes = {
  classes: PropTypes.shape({
    avatar: PropTypes.string.isRequired,
  }).isRequired,
  experiences: PropTypes.arrayOf(
    PropTypes.shape({}).isRequired,
  ).isRequired,
  images: PropTypes.shape({
    path: PropTypes.string.isRequired,
    repository: PropTypes.string.isRequired,
    server: PropTypes.string.isRequired,
  }).isRequired,
  maxYear: PropTypes.number.isRequired,
};

export default withStyles(styles)(ListItems);
