import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import List from '@material-ui/core/List';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
// Custom Components
import ListItems from './items';
// Utilities
import { getMaxYear, sorting } from '../../utilities/achievements';

const styles = theme => ({
  paper: {
    color: theme.palette.text.secondary,
    padding: theme.spacing.unit * 2,
    textAlign: 'justify',
  },
  root: {
    flexGrow: 1,
  },
});

const Achievements = (props) => {
  const { achievements, classes, images } = props;
  const maxYear = getMaxYear(achievements);

  achievements.sort(sorting.descending.both);

  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={1} md={2} />
        <Grid item xs={10} md={8}>
          <Paper className={classes.paper}>
            <Typography variant="headline" gutterBottom>
              Experience
            </Typography>
            <List>
              <ListItems
                experiences={achievements}
                images={images}
                maxYear={maxYear}
              />
            </List>
          </Paper>
        </Grid>
        <Grid item xs={1} md={2} />
      </Grid>
    </div>
  );
};

Achievements.propTypes = {
  achievements: PropTypes.arrayOf(
    PropTypes.shape({}).isRequired,
  ).isRequired,
  classes: PropTypes.shape({
    paper: PropTypes.string.isRequired,
    root: PropTypes.string.isRequired,
  }).isRequired,
  images: PropTypes.shape({}).isRequired,
};

export default withStyles(styles)(Achievements);
