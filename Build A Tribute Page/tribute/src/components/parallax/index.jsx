import React from 'react';
import PropTypes from 'prop-types';
import validation from '../../utilities/validation/parallax';

const Parallax = (props) => {
  const {
    alpha,
    blue,
    children,
    green,
    heightUnit,
    heightValue,
    image,
    red,
    showForeground,
  } = props;

  const checkRed = validation.color(red);
  const checkGreen = validation.color(green);
  const checkBlue = validation.color(blue);
  const checkAlpha = validation.alpha(alpha);

  const styleParallax = `
    .parallax {
      background-image: url("${image}");

      height: ${heightValue}${heightUnit};

      background-attachment: fixed;
      background-position: center;
      background-repeat: no-repeat;
      background-size: cover;

      -webkit-transition: all .5s ease-in-out;
      -moz-transition: all .5s ease-in-out;
      transition: all .5s ease-in-out;
    }
  `;

  const styleForeground = `
    .parallax-foreground {
      background: rgb(${checkRed},${checkGreen},${checkBlue});
      background: rgba(${checkRed},${checkGreen},${checkBlue},${checkAlpha});
      height: 100%;
    }
  `;

  return (
    <React.Fragment>
      <style>
        {`
          ${styleParallax}
          ${showForeground ? styleForeground : ''}
        `}
      </style>
      <div
        className="parallax"
      >
        {showForeground
          ? (
            <div
              className="parallax-foreground"
            >
              {children}
            </div>
          ) : (
            <div>{children}</div>
          )
        }
      </div>
    </React.Fragment>
  );
};

Parallax.defaultProps = {
  alpha: 0.4,
  blue: 255,
  children: [],
  green: 255,
  red: 255,
  showForeground: true,
};

Parallax.propTypes = {
  alpha: PropTypes.number,
  blue: PropTypes.number,
  children: PropTypes.arrayOf(PropTypes.element),
  green: PropTypes.number,
  heightUnit: PropTypes.string.isRequired,
  heightValue: PropTypes.number.isRequired,
  image: PropTypes.string.isRequired,
  red: PropTypes.number,
  showForeground: PropTypes.bool,
};

export default Parallax;
