import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';

const styles = theme => ({
  paper: {
    color: theme.palette.text.secondary,
    padding: theme.spacing.unit * 2,
    textAlign: 'justify',
  },
  root: {
    flexGrow: 1,
  },
});

const FindOutMore = (props) => {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <Grid container spacing={24}>
        <Grid item xs={1} md={2} />
        <Grid item xs={10} md={8}>
          <Paper className={classes.paper}>
            <Typography
              variant="headline"
              gutterBottom
            >
              Find Out More
            </Typography>
            <Typography
              variant="body1"
              gutterBottom
            >
              {'You can find out more about Albano Mucci by '}
              <a
                id="tribute-link"
                href="https://en.wikipedia.org/wiki/Albano_Mucci"
                target="_blank"
                rel="noopener noreferrer"
              >
                {'reading this Wikipedia entry'}
              </a>
              {'.'}
            </Typography>
          </Paper>
        </Grid>
        <Grid item xs={1} md={2} />
      </Grid>
    </div>
  );
}

FindOutMore.propTypes = {
  classes: PropTypes.shape({
    paper: PropTypes.string.isRequired,
    root: PropTypes.string.isRequired,
  }).isRequired,
};

export default withStyles(styles)(FindOutMore);
