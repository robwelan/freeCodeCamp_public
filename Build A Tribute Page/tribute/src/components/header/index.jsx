import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
// Custom components
import Parallax from '../parallax';
import LazyLoadImages from '../lazy-load-images';
// Utilites
import getImageLocation from '../../utilities/images';

class PageHeader extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      backgrounds: [
        'corroboree',
        'tiger',
        'koala',
      ],
      current: 0,
    };
  }

  componentDidMount() {
    this.timerID = setInterval(
      () => this.tick(),
      3000,
    );
  }

  componentWillUnmount() {
    clearInterval(this.timerID);
  }

  tick() {
    this.setState((previousState) => {
      let id = previousState.current + 1;

      if (id > 2) {
        id = 0;
      }

      return {
        ...previousState,
        current: id,
      };
    });
  }

  render() {
    const {
      images,
      name,
      organization,
      title,
    } = this.props;
    const { current, backgrounds } = this.state;
    const imageLocation = getImageLocation(images);
    const imageArray = [
      `${imageLocation}${images.profile.file}`,
      `${imageLocation}${images.corroboree}`,
      `${imageLocation}${images.tiger}`,
      `${imageLocation}${images.koala}`,
    ];

    return (
      <section id="header">

        <LazyLoadImages images={imageArray} />

        <Parallax
          image={
            `${imageLocation}${images[backgrounds[current]]}`
          }
          heightValue={550}
          heightUnit="px"
          alpha={0.5}
        >
          <Typography
            id="title"
            align="center"
            gutterBottom
            variant="display4"
          >
            {
              `${name.first}${' '}${name.last}`
            }
          </Typography>
          <figure id="img-div">
            <img
              id="image"
              className="profile-image"
              src={
                `${imageLocation}${images.profile.file}`
              }
              alt={images.profile.alt}
            />
            <figcaption id="img-caption">
              <Typography variant="headline" gutterBottom>
                {
                  `${name.first}${' '}${name.last}`
                }
              </Typography>
              <Typography variant="title" gutterBottom>
                {
                  `${title}`
                }
              </Typography>
              <Typography variant="subheading" gutterBottom>
                {
                  `${organization}`
                }
              </Typography>
            </figcaption>
          </figure>
        </Parallax>
      </section>
    );
  }
}

PageHeader.propTypes = {
  images: PropTypes.shape({
    corroboree: PropTypes.string.isRequired,
    koala: PropTypes.string.isRequired,
    path: PropTypes.string.isRequired,
    profile: PropTypes.shape({
      alt: PropTypes.string.isRequired,
      file: PropTypes.string.isRequired,
    }).isRequired,
    repository: PropTypes.string.isRequired,
    server: PropTypes.string.isRequired,
    tiger: PropTypes.string.isRequired,
  }).isRequired,
  name: PropTypes.shape({
    first: PropTypes.string.isRequired,
    last: PropTypes.string.isRequired,
  }).isRequired,
  organization: PropTypes.string.isRequired,
  title: PropTypes.string.isRequired,
};

export default PageHeader;
