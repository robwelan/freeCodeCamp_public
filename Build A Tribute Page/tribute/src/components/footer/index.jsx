import React from 'react';
import {
  faGithub,
  faGitlab,
  faTwitter,
  faGooglePlus,
  faLinkedin,
  faFreeCodeCamp,
} from '@fortawesome/free-brands-svg-icons';
// Custom components
import ProfileLink from './profile-link';
// Custom styles
import 'materialize-css/dist/css/materialize.min.css';
import './index.css';

const Footer = () => (
  <footer className="page-footer">
    <div className="container">
      <div className="row">
        <div className="col l6 s12">
          <h5>About The Developer</h5>
          <p>I&#x00027;m a JavaScript enthusiast who:</p>
          <ul>
            <li>a) teaches JavaScript, CSS and HTML to others</li>
            <li>b) develops Apps and web sites using cool tech</li>
          </ul>
          <p>&nbsp;</p>
          <p>
            You can find the public repo for this project here:
          </p>
          <ul>
            <li>
              <ProfileLink
                href="https://gitlab.com/robwelan/freeCodeCamp_public"
                icon={faGitlab}
                label="Build A Tribute Page"
              />
            </li>
          </ul>
        </div>
        <div className="col l4 offset-l2 s12">
          <h5>Ways To Contact Me</h5>
          <ul>
            <li>
              <ProfileLink
                href="https://twitter.com/RobWelan"
                icon={faTwitter}
                label="Twitter"
              />
            </li>
            <li>
              <ProfileLink
                href="https://plus.google.com/u/0/+RobWelan"
                icon={faGooglePlus}
                label="Google+"
              />
            </li>
            <li>
              <ProfileLink
                href="https://github.com/robwelan"
                icon={faGithub}
                label="GitHub"
              />
            </li>
            <li>
              <ProfileLink
                href="https://gitlab.com/robwelan"
                icon={faGitlab}
                label="GitLab"
              />
            </li>
            <li>
              <ProfileLink
                href="https://www.linkedin.com/in/robwelan"
                icon={faLinkedin}
                label="LinkedIn"
              />
            </li>
            <li>
              <ProfileLink
                href="https://www.freecodecamp.com/robwelan"
                icon={faFreeCodeCamp}
                label="freeCodeCamp"
              />
            </li>
          </ul>
        </div>
      </div>
    </div>
    <div className="footer-copyright">
      <div className="container">
        {'© 2018 Rob Welan | '}
        <a
          href="https://creatureoftech.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
          {'Creature of Tech'}
        </a>
        {' | Built with '}
        <a
          href="https://material-ui.com/"
          target="_blank"
          rel="noopener noreferrer"
        >
          {'Material-UI React'}
        </a>
      </div>
    </div>
  </footer>
);

export default Footer;
