import React from 'react';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import PropTypes from 'prop-types';

const ProfileLink = (props) => {
  const { href, icon, label } = props;

  return (
    <a
      href={href}
      target="_blank"
      rel="noopener noreferrer"
    >
      <FontAwesomeIcon
        icon={icon}
        aria-hidden="true"
      />
      {` ${label}`}
    </a>
  );
};

ProfileLink.propTypes = {
  href: PropTypes.string.isRequired,
  icon: PropTypes.shape().isRequired,
  label: PropTypes.string.isRequired,
};

export default ProfileLink;
