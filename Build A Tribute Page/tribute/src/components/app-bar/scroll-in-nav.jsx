// based on this gist:
// https://gist.github.com/brthornbury/27531e4616b68131e512fc622a61baba

import React from 'react';
import PropTypes from 'prop-types';
import raf from 'raf';
// Custom styles
import './index.css';

const getScrollY = () => {
  let scroll = 0;

  if (window.pageYOffset !== undefined) {
    scroll = window.pageYOffset;
  } else if (window.scrollTop !== undefined) {
    scroll = window.scrollTop;
  } else {
    scroll = (window.document.documentElement
      || window.document.body.parentNode
      || window.document.body).scrollTop;
  }

  return scroll;
};

class ScrollInNav extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      hidden: true,
    };

    this.scrollnav = React.createRef;

    this.handlingScrollUpdate = false;
    this.handleScroll = this.handleScroll.bind(this);
    this.update = this.update.bind(this);
  }

  componentDidMount() {
    window.addEventListener('scroll', this.handleScroll, true);
  }

  componentWillUnmount() {
    window.removeEventListener('scroll', this.handleScroll, true);
  }

  handleScroll() {
    if (!this.handlingScrollUpdate) {
      this.handlingScrollUpdate = true;
      raf(this.update);
    }
  }

  update() {
    const { scrollInHeight } = this.props;
    const currentScrollY = getScrollY();

    this.setState({
      hidden: currentScrollY < scrollInHeight,
    });

    this.handlingScrollUpdate = false;
  }

  render() {
    const { hidden } = this.state;
    const { children } = this.props;
    const classes = [
      'scroll-in-nav',
    ];

    if (hidden) {
      classes.push('scroll-hidden');
    } else {
      classes.push('scrolled-in');
    }

    return (
      <div ref={this.scrollnav}>
        <div className={classes.join(' ').trim()}>
          {children}
        </div>
      </div>
    );
  }
}

ScrollInNav.defaultProps = {
  scrollInHeight: 50,
};

ScrollInNav.propTypes = {
  children: PropTypes.shape({}).isRequired,
  scrollInHeight: PropTypes.number,
};

export default ScrollInNav;
