import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
// Custom styles
import './index.css';

const styles = {
  root: {
    flexGrow: 1,
  },
};

function TributeAppBar(props) {
  const { classes, data: { name } } = props;
  return (
    <div className={classes.root}>
      <AppBar position="fixed" color="primary">
        <Toolbar>
          <Typography variant="title" color="inherit" className="app-title">
            {`
            ${name.first}
            ${' '}
            ${name.last}
            `}
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
}

TributeAppBar.propTypes = {
  classes: PropTypes.shape({}).isRequired,
  data: PropTypes.shape({
    name: PropTypes.shape({
      first: PropTypes.string.isRequired,
      last: PropTypes.string.isRequired,
    }).isRequired,
  }).isRequired,
};

export default withStyles(styles)(TributeAppBar);
