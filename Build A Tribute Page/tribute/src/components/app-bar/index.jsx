import React from 'react';
import ScrollInNav from './scroll-in-nav';
import TributeAppBar from './tribute-app-bar';

const TopBar = props => (
  <ScrollInNav scrollInHeight={400}>
    <TributeAppBar data={props} />
  </ScrollInNav>
);

export default TopBar;
