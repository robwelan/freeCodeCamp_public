import React from 'react';
import Index from './pages/index';
import data from './data';

import './App.css';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = data;
  }

  render() {
    return (
      <Index data={this.state} />
    );
  }
}

export default App;
