import story from './story';

const data = {
  name: {
    first: 'Albano',
    last: 'Mucci',
  },
  title: 'General Manager Life Sciences and Dreamworld Corroboree',
  organization: 'Dreamworld Wildlife Foundation',
  images: {
    server: 'https://gitlab.com/',
    repository: 'robwelan/freeCodeCamp_public/raw/master/Build%20A%20Tribute%20Page/tribute/',
    path: 'src/images/',
    profile: {
      file: 'profile_withTiger.jpg',
      alt: 'Albano Mucci sitting with a tiger cub',
    },
    corroboree: 'corroboree.jpg',
    tiger: 'tiger.jpg',
    koala: 'koala.jpg',
  },
  story,
  awards: [
    {
      year: 2014,
      award: 'Winner QLD Premier\'s Reconciliation Award',
      by: 'Queensland Government',
    },
    {
      year: 2017,
      award: 'QTIC Indigenous Champion',
      by: 'Queensland Tourism Industry Council',
    },
    {
      year: 2016,
      award: 'Business Events Ambassador',
      by: 'Gold Coast Ambassadors Circle Mayoral Awards',
    },
  ],
  experience: [
    {
      start: { year: 2005 },
      end: { year: 2019 },
      logo: {
        file: 'logo-dreamworld.png',
        alt: 'Dreamworld Logo',
      },
      acomplishment: {
        title: 'GM Corroboree / Tiger Island / Conservation',
        activity: 'Delivered a first-of-its-kind anywhere in the world - a world class Aboriginal and Torres Strait Islander experience intertwined with live animal exhibits and interactions.',
      },
    },
    {
      start: { year: 2012 },
      end: { year: 2019 },
      logo: {
        file: 'logo-dwf.png',
        alt: 'Dreamworld Wildlife Foundation Logo',
      },
      acomplishment: {
        title: 'GM / Director Dreamworld Wildlife Foundation',
        activity: 'The Dreamworld Wildlife Foundation (DWF) is a vehicle for preserving rare and endangered wildlife, both indigenous to Australia and from abroad. Behind the scenes, Albano leads the DWF by working tirelessly to assist in the preservation of breeding stocks of tigers and delivering a deeper understanding of the breeding habits of koalas.',
      },
    },
    {
      start: { year: 2015 },
      end: { year: 2018 },
      logo: {
        file: 'logo-gccg2018.png',
        alt: 'Gold Coast Commonwealth Games 2018 Logo',
      },
      acomplishment: {
        title: 'Indigenous Relationships Manager, Gold Coast 2018 Commonwealth Games Corporation',
        activity: 'Albano Worked in a priviliged position working with the local Aboriginal and Torres Strait Islander community to develop and implement a reconciliation action plan and by making sure all areas of the Commonwealth Games were touched by Aboriginal and Torres Strait Isalnder culture.',
      },
    },
    {
      start: { year: 2001 },
      end: { year: 2005 },
      logo: {
        file: 'logo-arp.png',
        alt: 'Australian Reptile Park Logo',
      },
      acomplishment: {
        title: 'General Manager, Australian Reptile Park',
        activity: 'Developer of new interactive wildlife exhibits and interactions.',
      },
    },
    {
      start: { year: 1997 },
      end: { year: 2001 },
      logo: {
        file: 'logo-fcc.png',
        alt: 'Fairfield City Council Logo',
      },
      acomplishment: {
        title: 'Operations Manager, City of Fairfield City Farm Attraction',
        activity: '',
      },
    },
  ],
};

export default data;
