const sorting = {
  descending(a, b) {
    let v = 0;

    if (a.year < b.year) {
      v = 1;
    }

    if (a.year > b.year) {
      v = -1;
    }
    return v;
  },
};

export default sorting;
