const getImageLocation = images => `${images.server}${images.repository}${images.path}`;

export default getImageLocation;
