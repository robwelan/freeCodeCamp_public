const getMaxYear = array => Math.max(...array.map(o => o.end.year));

const sorting = {
  descending: {
    both(a, b) {
      let v = 0;

      if (`${a.end.year}${a.start.year}` < `${b.end.year}${b.start.year}`) {
        v = 1;
      }

      if (`${a.end.year}${a.start.year}` > `${b.end.year}${b.start.year}`) {
        v = -1;
      }
      return v;
    },
    end(a, b) {
      let v = 0;

      if (a.end.year < b.end.year) {
        v = 1;
      }

      if (a.end.year > b.end.year) {
        v = -1;
      }
      return v;
    },
    start(a, b) {
      let v = 0;

      if (a.start.year < b.start.year) {
        v = 1;
      }

      if (a.start.year > b.start.year) {
        v = -1;
      }
      return v;
    },
  },
};

export { getMaxYear, sorting };
