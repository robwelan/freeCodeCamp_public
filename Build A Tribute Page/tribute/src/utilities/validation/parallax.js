const validation = {
  default: {
    color: 255,
    alpha: 0.4,
  },
  color(color) {
    let validColor = this.default.color;

    if (!Number.isNaN(color)) {
      if (color < 0) {
        validColor = 0;
      } else if (color > 255) {
        validColor = 255;
      } else {
        validColor = color;
      }
    }

    return validColor;
  },
  alpha(alpha) {
    let validAlpha = this.default.alpha;

    if (!Number.isNaN(alpha)) {
      if (alpha < 0) {
        validAlpha = 0;
      } else if (alpha > 1) {
        validAlpha = 1;
      } else {
        validAlpha = alpha;
      }
    }

    return validAlpha;
  },
};

export default validation;
