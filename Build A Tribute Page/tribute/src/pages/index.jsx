import React from 'react';
import PropTypes from 'prop-types';
import CssBaseline from '@material-ui/core/CssBaseline';
// Custom components
import TributeAppBar from '../components/app-bar';
import Awards from '../components/awards';
import Achievements from '../components/achievements';
import FindOutMore from '../components/find-out-more';
import Footer from '../components/footer';
import Header from '../components/header';
import Story from '../components/story';

const Index = (props) => {
  const {
    data: {
      awards,
      experience,
      images,
      name,
      organization,
      story,
      title,
    },
  } = props;

  return (
    <React.Fragment>
      <TributeAppBar name={name} />
      <CssBaseline />
      <article id="main">

        <Header
          name={name}
          title={title}
          organization={organization}
          images={images}
        />

        <section id="tribute-info">
          <Story story={story} />
          <Achievements
            achievements={experience}
            images={images}
          />
          <Awards
            awards={awards}
          />
        </section>

        <section id="find-out-more">
          <FindOutMore />
        </section>
      </article>
      <Footer />
    </React.Fragment>
  );
};

Index.propTypes = {
  data: PropTypes.shape({
    name: PropTypes.shape({
      first: PropTypes.string.isRequired,
      last: PropTypes.string.isRequired,
    }).isRequired,
    title: PropTypes.string.isRequired,
    organization: PropTypes.string.isRequired,
    images: PropTypes.shape({}).isRequired,
    story: PropTypes.string.isRequired,
  }).isRequired,
};

export default Index;
